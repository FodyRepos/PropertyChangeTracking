//namespace ConsoleApp
//{
//    [MethodAttributeAspect]
//    public class PublicMethodAttributeAspectMethods
//    {
//        public static string PublicMethod() => ProtectedInternalMethod();

//        protected internal static string ProtectedInternalMethod() => PrivateProtectedMethod();

//        private protected static string PrivateProtectedMethod() => InternalMethod();

//        internal static string InternalMethod() => ProtectedMethod();

//        protected static string ProtectedMethod() => PrivateMethod();

//        private static string PrivateMethod() => string.Empty;
//    }
//}