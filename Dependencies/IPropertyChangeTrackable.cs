﻿using System.Collections.Generic;

public interface IPropertyChangeTrackable
{
    Dictionary<string, bool> ChangeProperties { get; set; }
    bool IsTracking { get; set; }
}
