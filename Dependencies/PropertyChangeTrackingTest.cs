﻿using System;
    [PropertyChangeTracking]
    public class PropertyChangeTrackingTest2
    {
        public DateTime? Prop1 { get; set; }
        public string Test2 { get; set; }
        public int IntVal1 { get; set; }
        public int? IntVal2 { get; set; }
    }