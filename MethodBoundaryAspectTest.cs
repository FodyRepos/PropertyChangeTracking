﻿using System.Runtime.CompilerServices;

using Mono.Cecil;
using Mono.Cecil.Cil;

namespace ConsoleApp
{
    public class MethodBoundaryAspectTest : PublicMethodAttributeAspectMethods
    {
        [MethodAttributeAspect]
        public void MyProperty()
        {
            Console.WriteLine(1111);
        }

        public void MethodMain()
        {
            Console.WriteLine("Hello World");
        }

        public static void Test() 
        {
            using var assembly = AssemblyDefinition.ReadAssembly(typeof(MethodBoundaryAspectTest).Assembly.Location);

            //AssemblyDefinition assembiy = AssemblyDefinition.ReadAssembly(Path);//Path: dll or exe Path
            var method = assembly.MainModule
              .Types.First(t => t.Name == "MethodBoundaryAspectTest")
              .Methods.First(m => m.Name == "MethodMain");

            {
                method.Parameters.Add(
                    new ParameterDefinition(
                        "memberName", 
                        ParameterAttributes.Optional | ParameterAttributes.HasDefault, 
                        assembly.MainModule.TypeSystem.String));

                CustomAttribute attribute = new CustomAttribute(
                    assembly.MainModule.ImportReference(
                        typeof(CallerMemberNameAttribute).GetConstructor(Type.EmptyTypes)
                ));

                method.Parameters[^1].CustomAttributes.Add(attribute);
                method.Parameters[^1].Constant = "xx";
            }

            var worker = method.Body.GetILProcessor();//Get IL

            string FrontTime = DateTime.Now.ToString();
            var ins = method.Body.Instructions[0];//Get First IL Step
            worker.InsertBefore(ins, worker.Create(OpCodes.Ldstr, FrontTime));
            worker.InsertBefore(ins, worker.Create(OpCodes.Call,
            assembly.MainModule.Import(typeof(Console).GetMethod("WriteLine", new Type[] { typeof(string) }))));

            string BackTime = DateTime.Now.ToString();
            ins = method.Body.Instructions[method.Body.Instructions.Count - 1]; //Get Lastest IL Step
            worker.InsertBefore(ins, worker.Create(OpCodes.Ldstr, BackTime+"sss"));
            worker.InsertBefore(ins, worker.Create(OpCodes.Call,
            assembly.MainModule.Import(typeof(Console).GetMethod("WriteLine", new Type[] { typeof(string) }))));

            if (!System.IO.File.Exists(typeof(MethodBoundaryAspectTest).Assembly.Location.Replace("MethodAop.dll", "MethodAopX.dll")))
            {
                System.IO.File.Create(typeof(MethodBoundaryAspectTest).Assembly.Location.Replace("MethodAop.dll", "MethodAopX.dll")).Dispose();
            }
            assembly.Write(typeof(MethodBoundaryAspectTest).Assembly.Location.Replace("MethodAop.dll", "MethodAopX.dll"));
        }

    }
}