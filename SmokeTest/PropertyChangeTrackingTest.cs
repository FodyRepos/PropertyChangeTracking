﻿[PropertyChangeTracking]
public class PropertyChangeTrackingTest
{
    public DateTime? Prop1 { get; set; }
    public string Test2 { get; set; }
    public int IntVal1 { get; set; }
    public int? IntVal2 { get; set; }
}


public class TrackChange2 : PropertyChangeTrackingTest
{
    public int MyProperty { get; set; }
}
